# Handy utilities for network administrators

This is a collection of small utilities that may be useful to a network
adminstrator or anyone that tests networks and networking gear.

# The Utilities

## gen-ipv6-ula

A script that generates an ipv6 unique local address which can be used
similarly to RFC 1918 addresses for site local private addresses. Some
will say you should not use ULAs, but it has it's uses, particulary in
microservice enviroments where traffic between services never leaves the
private cloud.

